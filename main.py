import csv
import os

import cv2
import numpy as np
import matplotlib.pyplot as plt
import math
import pandas as pd


def canny(img):
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    kernel = 5
    blur = cv2.GaussianBlur(gray, (kernel, kernel), 0)
    canny = cv2.Canny(blur, 50, 250)
    return canny


def region_of_interest(img):
    height = img.shape[0]
    width = img.shape[1]
    mask = np.zeros_like(img)
    polygon = np.array([[(30, 200), (100, 140), (135, 140), (200, 200)]], np.int32)
    cv2.fillPoly(mask, polygon, (255, 255, 255))
    mask_image = cv2.bitwise_and(img, mask)
    return mask_image


def houghLines(img):
    houghLines = cv2.HoughLinesP(img, 2, np.pi / 180, 20, np.array([]), minLineLength=10, maxLineGap=150)
    return houghLines


def display_lines(img, lines):
    line_image = np.zeros_like(img)
    if lines is not None:
        for line in lines:
            for x1, y1, x2, y2 in line:
                if (0 < x1) & (x1 < 2000) & (0 < x2) & (x2 < 2000):
                    cv2.line(img, (x1, y1), (x2, y2), (255, 0, 0), 2)
    return img


def make_point(img, lineSI):
    slope, intercept = lineSI
    height = img.shape[0]
    y1 = int(height)
    y2 = int(y1 * 3 / 5)
    x1 = int((y1 - intercept) / slope)
    x2 = int((y2 - intercept) / slope)
    return [[x1, y1, x2, y2]]


def average_slope_intercept(img, lines):
    left_fit = []
    right_fit = []
    if lines is not None:
        for line in lines:
            for x1, y1, x2, y2 in line:
                fit = np.polyfit((x1, x2), (y1, y2), 1)
                slope = fit[0]
                intercept = fit[1]
                if slope < 0:
                    left_fit.append((slope, intercept))
                else:
                    right_fit.append((slope, intercept))

        left_fit_average = np.average(left_fit, axis=0)
        right_fit_average = np.average(right_fit, axis=0)
        left_line = make_point(img, left_fit_average)
        right_line = make_point(img, right_fit_average)
        average_lines = [left_line, right_line]
        return average_lines


x1 = []
y1 = []
x2 = []
y2 = []
x3 = []
y3 = []
x4 = []
y4 = []
data_file = []

capture = cv2.VideoCapture('test5.mp4')
currentframe = 0
while capture.isOpened():
    try:
        ret, image = capture.read()
        if ret:
            frame = np.copy(image)
            frame = cv2.resize(frame, (224, 224))
            frame = np.asarray(frame)

            # canny edge detector
            canny_output = canny(frame)

            # region of interest
            mask_output = region_of_interest(canny_output)

            # probabilistic hough transform
            lines = houghLines(mask_output)
            average_lines = average_slope_intercept(mask_output, lines)
            a = np.c_[average_lines[0], average_lines[1]]  # output is [[ array ]]array

            cv2.imwrite('/content/gdrive/MyDrive/model_checkpoint/dataframe/frame' + str(currentframe) + '.png', image)
            name = 'frame' + str(currentframe) + '.png'

            data_file.append(name)
            x1.append(a[0][0])
            y1.append(a[0][1])
            x2.append(a[0][2])
            y2.append(a[0][3])
            x3.append(a[0][4])
            y3.append(a[0][5])
            x4.append(a[0][6])
            y4.append(a[0][7])

            print(currentframe)
            currentframe += 1
            if cv2.waitKey(25) & 0xff == ord('q'):
                break
        else:
            break

    except Exception:
        pass

capture.release()
cv2.destroyAllWindows()

df = {'name': data_file,
      'x1': x1,
      'y1': y1,
      'x2': x2,
      'y2': y2,
      'x3': x3,
      'y3': y3,
      'x4': x4,
      'y4': y4,
      }

df = pd.DataFrame(df, columns=['name', 'x1', 'y1', 'x2', 'y2', 'x3', 'y3', 'x4', 'y4'])
# print(len(data_file), ' ', len(coordinates))
# df1 = df.sample(frac=1, replace=True)
df.to_csv('data.csv', index=False)
print('Data:', df)
