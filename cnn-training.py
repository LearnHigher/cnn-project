from keras.models import Model
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Dropout
from keras.layers.core import Lambda
from keras.layers.core import Dense
from keras.layers import Flatten
from keras.layers import Input

import plotly.graph_objects as go

import matplotlib.pyplot as plt
import numpy as np

from tqdm import tqdm
import pandas as pd

from tensorflow.keras.preprocessing import image

import PIL.Image
from sklearn.model_selection import train_test_split


# Build model
def build_model():
    input = Input(shape=(224, 224, 3))
    x = Conv2D(32, (3, 3), padding="same")(input)
    x = Activation("relu")(x)
    x = MaxPooling2D(pool_size=(3, 3))(x)
    x = Dropout(0.25)(x)
    x = BatchNormalization()(x)

    x = Conv2D(32, (3, 3), padding="same")(x)
    x = Activation("relu")(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Dropout(0.25)(x)
    x = BatchNormalization()(x)

    x = Conv2D(64, (3, 3), padding="same")(x)
    x = Activation("relu")(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Dropout(0.25)(x)
    x = BatchNormalization()(x)

    x = Conv2D(128, (3, 3), padding="same")(x)
    x = Activation("relu")(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Dropout(0.25)(x)
    x = BatchNormalization()(x)

    x = Flatten()(x)
    x = Dense(256)(x)
    x = Activation("relu")(x)

    x1 = Dense(1)(x)
    x1 = Activation("linear", name='x1')(x1)

    y1 = Dense(1)(x)
    y1 = Activation("linear", name='y1')(y1)

    x2 = Dense(1)(x)
    x2 = Activation("linear", name='x2')(x2)

    y2 = Dense(1)(x)
    y2 = Activation("linear", name='y2')(y2)

    x3 = Dense(1)(x)
    x3 = Activation("linear", name='x3')(x3)

    y3 = Dense(1)(x)
    y3 = Activation("linear", name='y3')(y3)

    x4 = Dense(1)(x)
    x4 = Activation("linear", name='x4')(x4)

    y4 = Dense(1)(x)
    y4 = Activation("linear", name='y4')(y4)

    model = Model(inputs=input,
                  outputs=[x1, y1, x2, y2, x3, y3, x4, y4])

    return model


model = build_model()
print(model.summary())

# Data preprocessing
df = pd.read_csv('data.csv')
print(df.describe)
print(df.shape)

train_image = []
for i in tqdm(range(df.shape[0])):
    img = image.load_img('./dataframe/frame' + str(i)+ '.png', target_size=(224,224,3))
    img = image.img_to_array(img)
    img = img/255
    train_image.append(img)
X = np.array(train_image)
print('Load image complete!')

y = np.array(df.drop(['name'],axis=1))
print(y.shape)

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1, test_size=0.2)
print('Split data complete')

# Train model
print('Training model')

init_lr = 1e-4
epochs = 2000
opt = Adam(learning_rate=init_lr, decay=init_lr / epochs)

model.compile(
    optimizer = opt,
    loss='mean_squared_logarithmic_error',
)

callbacks = [
    ModelCheckpoint("best_model.h5", monitor='val_loss')
]
history = model.fit(X_train,
                      {'x1': y_train[:,0],
                       'y1': y_train[:,1],
                       'x2': y_train[:,2],
                       'y2': y_train[:,3],
                       'x3': y_train[:,4],
                       'y3': y_train[:,5],
                       'x4': y_train[:,6],
                       'y4': y_train[:,7]},
                      epochs=2000,
                      callbacks=callbacks,
                      validation_data=(X_test,
                                       {'x1': y_test[:,0],
                                        'y1': y_test[:,1],
                                        'x2': y_test[:,2],
                                        'y2': y_test[:,3],
                                        'x3': y_test[:,4],
                                        'y3': y_test[:,5],
                                        'x4': y_test[:,6],
                                        'y4': y_test[:,7]}
))
model.save('best_model.h5')
# Plotting
fig = go.Figure()
fig.add_trace(go.Scattergl(
                    y=history.history['loss'],
                    name='Train'))
fig.add_trace(go.Scattergl(
                    y=history.history['val_loss'],
                    name='Valid'))
fig.update_layout(height=500,
                  width=700,
                  title='Overall loss',
                  xaxis_title='Epoch',
                  yaxis_title='Loss')
fig.show()