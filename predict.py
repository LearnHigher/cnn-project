import tensorflow as tf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import glob
import cv2
from tensorflow import keras
from keras.models import load_model

new_model = load_model('best_model_3.h5')
capture = cv2.VideoCapture('test5.mp4')
while capture.isOpened():
    ret, image = capture.read()
    if ret:
        frame = np.copy(image)
        frame = cv2.resize(frame, (224, 224))
        im = np.copy(frame)
        frame = frame / 255
        frame = np.asarray([frame])

        result = (new_model.predict(frame))
        x1 = int(result[0])
        y1 = int(result[1])
        x2 = int(result[2])
        y2 = int(result[3])
        x3 = int(result[4])
        y3 = int(result[5])
        x4 = int(result[6])
        y4 = int(result[7])

        img1 = cv2.circle(im, (x1, y1), radius=0, color=(0, 0, 255), thickness=10)
        img1 = cv2.circle(img1, (x2, y2), radius=0, color=(0, 255, 0), thickness=10)
        img1 = cv2.circle(img1, (x3, y3), radius=0, color=(255, 0, 0), thickness=10)
        img1 = cv2.circle(img1, (x4, y4), radius=0, color=(255, 255, 255), thickness=10)


        cv2.imshow('', img1)
        if cv2.waitKey(25) & 0xff == ord('q'):
          break
    else:
        break

cv2.destroyAllWindows()
capture.release()
